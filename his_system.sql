-- phpMyAdmin SQL Dump
-- version 3.5.8.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 05, 2014 at 05:58 PM
-- Server version: 5.5.34-0ubuntu0.13.04.1
-- PHP Version: 5.4.9-4ubuntu2.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `his_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE IF NOT EXISTS `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `barcode_number` varchar(255) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL,
  `quantity_type_id` int(11) NOT NULL DEFAULT '1',
  `price` int(11) NOT NULL,
  `storage_number` varchar(50) NOT NULL,
  `flags` tinyint(4) NOT NULL DEFAULT '0',
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `barcode_number`, `item_name`, `quantity`, `quantity_type_id`, `price`, `storage_number`, `flags`, `created_date`, `updated_date`, `created_by`, `updated_by`, `description`) VALUES
(1, '123456789abcde', 'Wrench', 20, 1, 500, 'a-1', 0, '2014-02-21 17:00:00', '2014-02-21 17:00:00', 1, 2, 'No description'),
(2, '123456789abcde', 'Pipe', 100, 1, 250, 'a-2', 1, '2014-02-21 00:00:00', '2014-02-21 00:00:00', 1, 0, 'None so far'),
(4, '123456789abcde', 'Fire Extinguisher', 50, 1, 3000, 'a-3', 0, '2014-02-21 07:19:00', '2014-02-21 07:19:00', 2, 1, 'Test description'),
(5, '4801278435121', 'Whiteboard Marker', 20, 1, 20, 'a-4', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 'This is a description'),
(6, '4801278435121', 'Whiteboard Marker', 20, 1, 20, 'a-4', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 'This is a simple decription'),
(7, '4801278435122', 'Marker', 20, 1, 20, 'a-5', 1, '2014-02-21 10:40:01', '2014-02-21 10:40:01', 0, 0, ''),
(8, '4801278435123', 'Marker', 20, 1, 20, 'a-4', 0, '2014-02-21 11:01:57', '2014-02-21 11:01:57', 0, 0, ''),
(9, '4801278435124', 'Whiteboard Marker', 20, 1, 20, 'a-4', 0, '2014-02-21 11:16:20', '2014-02-21 11:16:20', 0, 0, ''),
(10, '4801278435127', 'sample product', 20, 1, 20, 'a-4', 0, '2014-02-21 11:23:43', '2014-02-21 11:23:43', 0, 0, ''),
(11, '4801278435128', 'sample', 20, 1, 20, 'a-4', 0, '2014-02-21 11:45:15', '2014-02-21 11:45:15', 0, 0, ''),
(12, '4801278435129', 'Whiteboard Marker', 20, 1, 20, 'a-4', 0, '2014-02-21 11:54:16', '2014-02-21 11:54:16', 0, 0, ''),
(13, '4801278435130', 'Sample', 20, 1, 20, 'a-5', 0, '2014-02-21 17:54:58', '2014-02-21 17:54:58', 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE IF NOT EXISTS `logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `event_log` text NOT NULL,
  `is_new` int(1) NOT NULL DEFAULT '1' COMMENT 'This is to check if the log is new',
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=161 ;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `user_id`, `event_log`, `is_new`, `created_date`) VALUES
(1, 1, 'Google Translate now supports Latin, and I think they slipped in a joke. They knew that the first chunk of text anyone would try is some standard “lorem ipsum” text. Doing that, the first words are translated as “Hello World!”', 0, '2014-02-20 17:04:00'),
(2, 1, 'Nakahalin og tobo', 0, '2014-03-03 17:22:35'),
(8, 1, 'This is a new log', 0, '2014-03-04 18:04:19'),
(9, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(10, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(11, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(12, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(13, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(14, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(15, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(16, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(17, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(18, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(19, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(20, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(21, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(22, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(23, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(24, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(25, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(26, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(27, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(28, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(29, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(30, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(31, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(32, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(33, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(34, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(35, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(36, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(37, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(38, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(39, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(40, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(41, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(42, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(43, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(44, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(45, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(46, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(47, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(48, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(49, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(50, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(51, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(52, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(53, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(54, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(55, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(56, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(57, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(58, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(59, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(60, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(61, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(62, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(63, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(64, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(65, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(66, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(67, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(68, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(69, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(70, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(71, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(72, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(73, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(74, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(75, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(76, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(77, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(78, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(79, 1, 'This is a new log', 0, '2014-03-04 14:00:00'),
(80, 1, 'In publishing and graphic design, lorem ipsum[1] is a placeholder text commonly used to demonstrate the graphic elements of a document or visual presentation. By replacing the distraction of meaningful content with filler text of scrambled Latin it allows viewers to focus on graphical elements such as font, typography, and layout.\r\nThe lorem ipsum text is typically a mangled section of De finibus bonorum et malorum, a 1st-century BC Latin text by Cicero, with words altered, added, and removed that make it nonsensical, improper Latin.[1]', 0, '2014-03-05 11:54:00'),
(81, 1, 'In publishing and graphic design, lorem ipsum[1] is a placeholder text commonly used to demonstrate the graphic elements of a document or visual presentation. By replacing the distraction of meaningful content with filler text of scrambled Latin it allows viewers to focus on graphical elements such as font, typography, and layout.\r\nThe lorem ipsum text is typically a mangled section of De finibus bonorum et malorum, a 1st-century BC Latin text by Cicero, with words altered, added, and removed that make it nonsensical, improper Latin.[1]', 0, '2014-03-05 11:54:00'),
(82, 1, 'Lymuel Doming here', 0, '2014-03-05 11:54:00'),
(83, 1, 'Lymuel Doming here1', 0, '2014-03-05 11:54:00'),
(84, 1, 'Lymuel Doming here2', 0, '2014-03-05 11:54:00'),
(85, 1, 'Lymuel Doming here2', 0, '2014-03-05 11:54:00'),
(86, 1, 'Lymuel Doming', 0, '2014-03-05 11:54:00'),
(87, 2, 'Lymuel Doming', 0, '2014-03-05 11:54:00'),
(88, 1, 'Lymuel Doming', 0, '2014-03-05 11:54:00'),
(89, 1, 'Lymuel Doming', 0, '2014-03-05 11:54:00'),
(90, 1, 'Lymuel Doming', 0, '2014-03-05 11:54:00'),
(91, 2, 'Lymuel Doming', 0, '2014-03-05 11:54:00'),
(92, 1, 'Lymuel Doming', 0, '2014-03-05 11:54:00'),
(93, 2, 'Lymuel Doming', 0, '2014-03-05 11:54:00'),
(94, 2, 'Lymuel Doming', 0, '2014-03-05 11:54:00'),
(95, 1, 'Lymuel Doming', 0, '2014-03-05 11:54:00'),
(96, 1, 'Lymuel Doming is in here 1', 0, '2014-03-05 13:00:00'),
(97, 1, 'Lymuel Doming is in here 1', 0, '2014-03-05 13:01:00'),
(98, 2, 'Lymuel Doming is in here 3', 0, '2014-03-05 13:02:00'),
(99, 1, 'Lymuel Doming is in here 4', 0, '2014-03-05 13:03:00'),
(100, 2, 'Lymuel Doming is in here 3', 0, '2014-03-05 13:02:00'),
(101, 1, 'Lymuel Doming is in here 4', 0, '2014-03-05 13:03:00'),
(102, 2, 'Lymuel Doming is in here 3', 0, '2014-03-05 13:02:00'),
(103, 1, 'Lymuel Doming is in here 4', 0, '2014-03-05 13:03:00'),
(104, 2, 'Lymuel Doming is in here 3', 0, '2014-03-05 13:02:00'),
(105, 1, 'Lymuel Doming is in here 1', 0, '2014-03-05 13:04:00'),
(106, 2, 'Lymuel Doming is in here 3', 0, '2014-03-05 13:02:00'),
(107, 1, 'Lymuel Doming is in here 2', 0, '2014-03-05 13:05:00'),
(108, 2, 'Lymuel Doming is in here 3', 0, '2014-03-05 13:02:00'),
(109, 1, 'Lymuel Doming is in here 2', 0, '2014-03-05 13:06:00'),
(110, 2, 'Lymuel Doming is in here 3', 0, '2014-03-05 13:02:00'),
(111, 1, 'Lymuel Doming is in here 2', 0, '2014-03-05 13:07:00'),
(112, 2, 'Lymuel Doming is in here 3', 0, '2014-03-05 13:02:00'),
(113, 1, 'Lymuel Doming is in here 2', 0, '2014-03-05 13:07:00'),
(114, 2, 'Lymuel Doming is in here 3', 0, '2014-03-05 13:02:00'),
(115, 1, 'Lymuel Doming is in here 3', 0, '2014-03-05 13:09:00'),
(116, 1, 'Lymuel Doming is in here 3', 0, '2014-03-05 13:09:00'),
(117, 1, 'Lymuel Doming is in here 3', 0, '2014-03-05 13:09:00'),
(118, 1, 'Lymuel Doming is in here 3', 0, '2014-03-05 13:09:00'),
(119, 1, 'Lymuel Doming is in here 3', 0, '2014-03-05 13:09:00'),
(120, 1, 'Lymuel Doming is in here 3', 0, '2014-03-05 13:09:00'),
(121, 1, 'sample', 0, '2014-03-05 14:00:00'),
(122, 1, 'sample', 0, '2014-03-05 14:00:00'),
(123, 1, 'sample', 0, '2014-03-05 14:00:00'),
(124, 1, 'sample', 0, '2014-03-05 14:00:00'),
(125, 1, 'sample', 0, '2014-03-05 14:00:00'),
(126, 1, 'sample', 0, '2014-03-05 14:00:00'),
(127, 1, 'sample', 0, '2014-03-05 14:00:00'),
(128, 1, 'sample', 0, '2014-03-05 14:00:00'),
(129, 1, 'sample', 0, '2014-03-05 14:00:00'),
(130, 1, 'sample', 0, '2014-03-05 14:00:00'),
(131, 1, 'sample', 0, '2014-03-05 14:00:00'),
(132, 1, 'sample', 0, '2014-03-05 14:00:00'),
(133, 1, 'sample', 0, '2014-03-05 14:00:00'),
(134, 1, 'This is a sample log ulit', 0, '2014-03-05 15:45:06'),
(135, 2, 'This is a sample log ulit', 0, '2014-03-05 15:45:06'),
(136, 2, 'This is a sample log ulit', 0, '2014-03-05 15:45:06'),
(137, 2, 'This is a sample log ulit', 0, '2014-03-05 15:45:06'),
(138, 2, 'This is a sample log ulit', 0, '2014-03-05 15:45:06'),
(139, 2, 'This is a sample log ulit', 0, '2014-03-05 15:45:06'),
(140, 2, 'This is a sample log ulit', 0, '2014-03-05 15:45:06'),
(141, 2, 'This is a sample log ulit', 0, '2014-03-05 15:45:06'),
(142, 2, 'This is a sample log ulit', 0, '2014-03-05 15:45:06'),
(143, 2, 'This is a sample log ulit', 0, '2014-03-05 15:45:06'),
(144, 2, 'This is a sample log ulit', 0, '2014-03-05 15:45:06'),
(145, 2, 'This is a sample log ulit', 0, '2014-03-05 15:45:06'),
(146, 2, 'This is a sample log ulit', 0, '2014-03-05 15:45:06'),
(147, 2, 'This is a sample log ulit', 0, '2014-03-05 15:45:06'),
(148, 2, 'This is a sample log ulit', 0, '2014-03-05 15:45:06'),
(149, 2, 'This is a sample log ulit', 0, '2014-03-05 15:45:06'),
(150, 2, 'This is a sample log ulit', 0, '2014-03-05 15:45:06'),
(151, 2, 'This is a sample log ulit', 0, '2014-03-05 15:45:06'),
(152, 2, 'This is a sample log ulit', 0, '2014-03-05 15:45:06'),
(153, 2, 'This is a sample log ulit', 0, '2014-03-05 15:45:06'),
(154, 1, 'This is a sample log ulit', 0, '2014-03-05 15:45:06'),
(155, 1, 'This is a sample log ulit', 0, '2014-03-05 15:45:06'),
(156, 1, 'This is a sample log ulit', 0, '2014-03-05 15:45:06'),
(157, 1, 'This is a sample log ulit', 0, '2014-03-05 15:45:06'),
(158, 1, 'This is a sample log ulit', 0, '2014-03-05 15:45:06'),
(159, 1, 'This is a sample log ulit', 0, '2014-03-05 15:45:06'),
(160, 2, 'This is a sample log ulit', 0, '2014-03-05 15:45:06');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(50) NOT NULL,
  `body` text NOT NULL,
  `from` int(11) NOT NULL,
  `to` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `is_read` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `subject`, `body`, `from`, `to`, `created_date`, `updated_date`, `is_read`) VALUES
(1, 'Test Message', 'This is a test message to domingly', 1, 2, '2014-02-20 16:10:00', '2014-02-20 16:10:00', 0),
(2, 'This is a test message to Lymuel Doming', 'Test', 2, 1, '2014-02-20 16:16:00', '2014-02-20 16:16:00', 0),
(3, 'Sample 2', 'Test', 2, 1, '2014-02-20 16:18:00', '2014-02-20 16:18:00', 0),
(4, 'Sample 2', 'Test', 2, 1, '2014-02-20 16:18:00', '2014-02-20 16:18:00', 0),
(5, 'Sample 2', 'Test', 1, 2, '2014-02-20 16:18:00', '2014-02-20 16:18:00', 0),
(6, 'Sample 2', 'Test', 1, 2, '2014-02-20 16:18:00', '2014-02-20 16:18:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `quantity_types`
--

CREATE TABLE IF NOT EXISTS `quantity_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quantity_type` varchar(100) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `created_by_id` int(11) NOT NULL DEFAULT '0',
  `updated_by_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `quantity_types`
--

INSERT INTO `quantity_types` (`id`, `quantity_type`, `created_date`, `updated_date`, `created_by_id`, `updated_by_id`) VALUES
(1, 'Piece', '2014-02-21 17:28:00', '2014-02-21 17:28:00', 1, 0),
(2, 'Meter(s)', '2014-02-21 17:28:00', '2014-02-21 17:28:00', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `gender` tinyint(4) NOT NULL,
  `birth_date` date NOT NULL,
  `id_number` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `first_name`, `last_name`, `gender`, `birth_date`, `id_number`, `created_date`, `updated_date`, `is_deleted`) VALUES
(1, 'ldoming', '482256f2f8b16e75be646a31603dcd3ba4006db8', 'Lymuel', 'Doming', 0, '1993-12-27', '123456789', '2014-02-20 16:00:00', '2014-02-20 16:00:00', 0),
(2, 'domingly', '482256f2f8b16e75be646a31603dcd3ba4006db8', 'Doming', 'Lymuel', 0, '1993-12-27', '987654321', '2014-02-20 16:01:00', '2014-02-20 16:01:00', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
