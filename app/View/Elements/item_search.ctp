<div class="row">
    <div class="col-lg-12" style="text-align:right;">
        <?php
        echo $this->Form->create('Item', array(
            'action' => 'index',
            'class' => 'form-inline',
            'type' => 'get'
        ));
        ?>
        <div class="form-group">
            <?php
            echo $this->Form->input('string', array(
                'placeholder' => 'Search item...',
                'label' => false,
                'div' => false,
                'class' => 'form-control',
                'value' => $this->App->checkParam('string')
            ));
            ?>
            <button type="submit" class="btn btn-info">Search</button>
        </div>
    </div>
</div>