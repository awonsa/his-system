<div class="row">
    <div class="col-lg-5">
        <div class="col-lg-3" style="text-align: center;">
            <?php
            echo $this->Paginator->prev(
                    '« Previous', null, null, array('class' => 'disabled')
            );
            ?>
        </div>
        <div class="col-lg-6" style="text-align: center;">
            <?php
            echo $this->Paginator->numbers(array(
                'class' => 'btn btn-default pagination-button',
                'modulus' => 4
            ));
            ?>
        </div>
        <div class="col-lg-3" style="text-align: center;">
            <?php
            echo $this->Paginator->next(
                    'Next »', null, null, array('class' => 'disabled')
            );
            ?>
        </div>
    </div>
    <div class="col-lg-5"></div>
    <div class="col-lg-2" style="text-align: center;">
        <?php
        echo $this->Paginator->counter();
        ?>
    </div>
</div>