<style>
    #message_area{
        overflow-y: auto;
    }    
</style>

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.html">HIS System</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav side-nav">
            <li class="<?php echo $this->App->checkTabifActive('/logs/'); ?>">
                <?php
                echo $this->Html->link(
                        '<i class="fa fa-dashboard"></i> Dashboard', array('controller' => 'logs', 'action' => 'dashboard'), array('escape' => false)
                );
                ?>
            </li>
            <li class="<?php echo $this->App->checkTabifActive('/transactions/'); ?>">
                <?php
                echo $this->Html->link(
                        '<i class="fa fa-bar-chart-o"></i> Point-of-Sale', array('controller' => 'transactions', 'action' => 'pos'), array('escape' => false, 'target' => '_blank')
                );
                ?>
            </li>
            <li class="dropdown <?php echo $this->App->checkTabifActive('/items'); ?>">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-square-o-down"></i> Inventory <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li class="<?php echo $this->App->checkTabifActive('/items/index'); ?>">
                        <?php
                        echo $this->Html->link(
                                '<span class="glyphicon glyphicon-search"></span>  View/Search Items', array('controller' => 'items', 'action' => 'index'), array('escape' => false)
                        );
                        ?>
                    </li>
                    <li class="<?php echo $this->App->checkTabifActive('/items/add_item'); ?>">
                        <?php
                        echo $this->Html->link(
                                '<span class="glyphicon glyphicon-plus-sign"></span>  Add Item/s', array('controller' => 'items', 'action' => 'add_item'), array('escape' => false)
                        );
                        ?>
                    </li>
                </ul>
            </li>
        </ul>

        <ul class="nav navbar-nav navbar-right navbar-user">
            <li class="dropdown messages-dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> Messages <span class="badge" id="number_of_new_message_1">0</span> <b class="caret"></b></a>
                <ul class="dropdown-menu" id="message_area"></ul>
            </li>
            <li class="dropdown alerts-dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> Alerts <span class="badge">3</span> <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="#">Default <span class="label label-default">Default</span></a></li>
                    <li><a href="#">Primary <span class="label label-primary">Primary</span></a></li>
                    <li><a href="#">Success <span class="label label-success">Success</span></a></li>
                    <li><a href="#">Info <span class="label label-info">Info</span></a></li>
                    <li><a href="#">Warning <span class="label label-warning">Warning</span></a></li>
                    <li><a href="#">Danger <span class="label label-danger">Danger</span></a></li>
                    <li class="divider"></li>
                    <li><a href="#">View All</a></li>
                </ul>
            </li>
            <li class="dropdown user-dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Hello &nbsp;<i class="fa fa-user"></i> <?php echo $authUser['last_name'] . ', ' . $authUser['first_name'] ?> <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li>
                        <?php
                        echo $this->Html->link(
                                '<i class="fa fa-user"></i>  Profile', array('controller' => 'users', 'action' => 'profile'), array('escape' => false)
                        );
                        ?>
                    </li>
                    <li><a href="#"><i class="fa fa-envelope"></i> Inbox <span class="badge">7</span></a></li>
                    <li><a href="#"><i class="fa fa-gear"></i> Settings</a></li>
                    <li class="divider"></li>
                    <li>
                        <?php
                        echo $this->Html->link(
                                '<i class="fa fa-power-off"></i>  Log Out', array('controller' => 'users', 'action' => 'logout'), array('escape' => false)
                        );
                        ?>
                    </li>
                </ul>
            </li>
        </ul>
    </div><!-- /.navbar-collapse -->
</nav>



<script type="text/javascript">
    $(document).ready(function() {
        message_update()
        setInterval(function() {
            message_update()
        }, 5000);
    });


    function message_update() {
        $.ajax({
            type: "POST",
            url: "<?php echo $this->App->routes('messages', 'request_message_update'); ?>",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(data) {
                if (data.new_message_count !== 0) {
                    $('#number_of_new_message_1').html(data.new_message_count);
                } else {
                    $('#number_of_new_message_1').html('');
                }
                $('#message_area').html(data.message_preview);
            },
            error: function(data) {
                console.log(data);
            }
        });
    }
</script>


