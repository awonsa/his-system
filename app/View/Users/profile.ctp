<?php #pr($authUser); ?>
<div class="row">
    <div class="col-lg-12">
        <h1>Profile <small>User's Overview</small></h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <i class="fa fa-user"></i>&nbsp;<?php echo $authUser['last_name'] ?>'s profile page
            </div>
            <div class="panel-body">
                <ol class="breadcrumb">
                    <li><i class="fa fa-arrow-right"></i>&nbsp; Username : <?php echo $authUser['username'] ?></li>
                </ol>
                <ol class="breadcrumb">
                    <li><i class="fa fa-arrow-right"></i>&nbsp; Password : ****************</li>
                </ol>
                <ol class="breadcrumb">
                    <li><i class="fa fa-arrow-right"></i>&nbsp; Firstname : <?php echo $authUser['first_name'] ?></li>
                </ol>
                <ol class="breadcrumb">
                    <li><i class="fa fa-arrow-right"></i>&nbsp; Lastname : <?php echo $authUser['last_name'] ?></li>
                </ol>
                <ol class="breadcrumb">
                    <li><i class="fa fa-arrow-right"></i>&nbsp; Gender : <?php echo $this->App->gender($authUser['gender']); ?></li>
                </ol>
                <ol class="breadcrumb">
                    <li><i class="fa fa-arrow-right"></i>&nbsp; Birthdate  : <?php echo $authUser['birth_date'] ?></li>
                </ol>
                <ol class="breadcrumb">
                    <li><i class="fa fa-arrow-right"></i>&nbsp; Id number : <?php echo $authUser['id_number'] ?></li>
                </ol>
            </div>
        </div>
    </div>
</div>