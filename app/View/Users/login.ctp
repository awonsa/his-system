<div class="row">
    <div class="col-lg-4"></div>
    <div class="col-lg-4">
        </br></br>
        <?php echo $this->Session->flash(); ?>
        <?php echo $this->Form->create('User', array('class' => 'form-signin', 'role' => 'form')); ?>

        <h2 class="form-signin-heading">Please sign in</h2>
        <?php
        echo $this->Form->input('username', array('type' => 'username',
            'placeholder' => 'Username',
            'class' => 'form-control',
            'label' => 'Username'));
        echo $this->Form->input('password', array('type' => 'password',
            'placeholder' => 'Password',
            'class' => 'form-control'));
        ?>
        <label class="checkbox">
            <input type="checkbox" value="remember-me"> Remember me
        </label>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>

        <?php echo $this->Form->end(); ?>
    </div>
    <div class="col-lg-4"></div>
</div>
