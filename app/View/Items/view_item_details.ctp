<?php
$Items = $Items[0];
?>
<div class="table-responsive">
    <table class="table table-condensed" border="1">
        <thead style="font-weight: bold;">
            <tr>
                <td></td>
                <td>Item Informations</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td width="30%" style="font-weight: bold;">Barcode Number</td>
                <td><?php echo $Items['Item']['barcode_number']; ?></td>
            </tr>
            <tr>
                <td width="30%" style="font-weight: bold;">Item Name</td>
                <td><?php echo $Items['Item']['item_name']; ?></td>
            </tr>
            <tr>
                <td width="30%" style="font-weight: bold;">Quantity Remaining</td>
                <td><strong><?php echo $Items['Item']['quantity']; ?></strong></td>
            </tr>
            <tr>
                <td width="30%" style="font-weight: bold;">Price per Item</td>
                <td><?php echo number_format($Items['Item']['price'], 2); ?></td>
            </tr>
            <tr>
                <td width="30%" style="font-weight: bold;">Storage Area Number</td>
                <td><?php echo $Items['Item']['storage_number']; ?></td>
            </tr>
            <tr>
                <td width="30%" style="font-weight: bold;">Item Status</td>
                <td>
                    <?php if ($Items['Item']['flags'] == 0) { ?>
                        <div style="color:#21d600;font-weight:bold;">Normal</div>
                    <?php } else if ($Items['Item']['flags'] == 1) { ?>
                        <div style="color:#d69d00;font-weight:bold;">Almost out of stock</div>
                    <?php } else if ($Items['Item']['flags'] == 2) { ?>
                        <div style="color:red;font-weight:bold;">Out of stock</div>
                    <?php } ?>
                </td>
            </tr>
            <tr>
                <td width="30%" style="font-weight: bold;">Item added date</td>
                <td><?php echo $this->App->formatTime($Items['Item']['created_date']); ?></td>
            </tr>
            <tr>
                <td width="30%" style="font-weight: bold;">Item updated date</td>
                <td><?php echo $this->App->formatTime($Items['Item']['updated_date']); ?></td>
            </tr>
            <tr>
                <td width="30%" style="font-weight: bold;">Item added by</td>
                <td><?php echo (isset($Items['Created_by']['first_name'])) ? $Items['Created_by']['first_name'] . ' ' . $Items['Created_by']['last_name'] : '-'; ?></td>
            </tr>
            <tr>
                <td width="30%" style="font-weight: bold;">Item updated by</td>
                <td><?php echo (isset($Items['Updated_by']['first_name'])) ? $Items['Updated_by']['first_name'] . ' ' . $Items['Updated_by']['last_name'] : '-'; ?></td>
            </tr>
            <tr>
                <td width="30%" style="font-weight: bold;">Item description</td>
                <td><?php echo (isset($Items['Item']['description'])) ? $Items['Item']['description'] : '-'; ?></td>
            </tr>
            <tr>
                <td width="30%" style="font-weight: bold;">Actions</td>
                <td>
                    <?php
                    echo $this->Html->link(
                            '<span class="badge actions-edit"><i class="fa fa-edit"></i></span> Edit', array(
                        'action' => 'edit_item',
                        $Items['Item']['id']
                            ), array(
                        'escape' => false,
                        'title' => 'Edit this item'
                            )
                    );
                    ?>
                    &nbsp;|&nbsp;
                    <?php
                    echo $this->Html->link(
                            '<span class="badge actions-delete"><i class="fa fa-trash-o"></i></span> Delete', array('controller' => 'items',
                        'action' => 'delete_item',
                        $Items['Item']['id']
                            ), array(
                        'escape' => false,
                        'title' => 'Delete this item',
                        'confirm' => 'Are you sure you want to delete this item?'
                            )
                    );
                    ?>
                    &nbsp;|&nbsp;
                    <?php
                    echo $this->Html->link(
                            '<i class="fa fa-arrow-left"></i> Back', array('controller' => 'items',
                        'action' => 'index'
                            ), array(
                        'escape' => false,
                        'title' => 'Back to list'
                            )
                    );
                    ?>
                </td>
            </tr>
        </tbody>
    </table>
</div>
