<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <i class="fa fa-plus"></i>   Add Item(s)
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-3 item-form-wrapper">
                        <?php
                        echo $this->Form->create('Item', array('label' => false, 'class' => 'item_form'));
                        ?>
                        <div class="form-group">
                            <label><i class="fa fa-barcode"></i> Barcode</label>
                            <?php echo $this->Form->input('barcode_number', array('class' => 'form-control', 'label' => false)) ?>
                        </div>
                        <div class="form-group">
                            <label><i class="fa fa-trophy"></i> Item name</label>
                            <?php echo $this->Form->input('item_name', array('class' => 'form-control', 'label' => false)) ?>
                        </div>
                        <div class="form-group form-inline">
                            <label><i class="fa fa-shopping-cart"></i> Quantity</label></br>
                            <div class="row">
                                <div class="col-lg-6">
                                    <?php
                                    echo $this->Form->input('quantity', array(
                                        'class' => 'form-control',
                                        'type' => 'text',
                                        'label' => false,
                                        'div' => false,
                                        'style' => 'width:100%;'));
                                    ?>
                                </div>
                                <div class="col-lg-6">
                                    <?php
                                    echo $this->Form->input('quantity_type_id', array(
                                        'options' => $quantityType,
                                        'class' => 'form-control',
                                        'type' => 'select',
                                        'id' => 'selectquantitytype',
                                        'label' => false,
                                        'div' => false,
                                        'style' => 'width:100%;'));
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label><i class="fa fa-rub"></i> Price</label>
                            <div class="row">
                                <div class="col-md-6">
                                    <?php
                                    echo $this->Form->input('price', array(
                                        'class' => 'form-control',
                                        'type' => 'text',
                                        'label' => false,
                                        'div' => false));
                                    ?>
                                </div>
                                <div class="col-md-6" style="padding:10px;">
                                    <span id="quantitytype"></span>
                                </div>
                            </div>

                        </div>
                        <div class="form-group">
                            <label><i class="fa fa-hand-o-up"></i> Storage area info</label>
                            <?php echo $this->Form->input('storage_number', array('class' => 'form-control', 'label' => false)) ?>
                        </div>
                        <div class="form-group">
                            <label><i class="fa fa-comments-o"></i> Description</label>
                            <?php echo $this->Form->input('description', array(
                                'class' => 'form-control', 
                                'label' => false,
                                'style'=>'resize:none;')) ?>
                        </div>
                        <button type="submit" class="btn btn-info" style="float: right;"><i class="fa fa-plus-square"></i> Submit</button>
                        <?php echo $this->Form->end(); ?>
                    </div>
                    <div class="col-lg-9 item-carousel-wrapper">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="4"></li>
                            </ol>
                            <div class="carousel-inner">
                                <div class="item active">
                                    <img src="http://www.crumplepop.com/promo/formal-900x500.jpg" alt="First slide">
                                </div>
                                <div class="item">
                                    <img src="http://www.colin.org/Zircon/SellPages/ScanAndSensor/iSensor/Photos/iSensorFan900x500.jpg" alt="Second slide">
                                </div>
                                <div class="item">
                                    <img src="http://thedogpaddler.com/EmailNewsletters/080703_Outlaws_Panos_Vigilant/04_080629_BrilliantSunset_Pano08_900x500_opt9ADV.jpg" alt="Third slide">
                                </div>
                                <div class="item">
                                    <img src="http://www.chardontool.com/wordpress/wp-content/uploads/et_temp/DSCN2867-1556922_900x500.jpg" alt="Fourth slide">
                                </div>
                                <div class="item">
                                    <img src="http://www.bionutricio.com/images/stock_images/900x500_5.jpg" alt="Fifth slide">
                                </div>
                            </div>
                            <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">

    $(document).ready(function() {

        $('#quantitytype').text('/  ' + $('#selectquantitytype').find(":selected").text());

        $('#selectquantitytype').on('change', function() {
            $('#quantitytype').text('/  ' + $('#selectquantitytype').find(":selected").text());
        });


    });

</script>