<?php #pr($Items);                                                                                        ?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <i class="fa fa-search"></i>   View / Search Items
            </div>
            <div class="panel-body">
                <?php if (isset($enable_search)): ?>
                    <div class="row">
                        <div class="col-lg-12" style="text-align:right;">
                            <?php
                            echo $this->Form->create('Item', array(
                                'action' => 'index',
                                'class' => 'form-inline',
                                'type' => 'get'
                            ));
                            ?>
                            <div class="form-group">
                                <?php
                                echo $this->Form->input('string', array(
                                    'placeholder' => 'Search something...',
                                    'label' => false,
                                    'div' => false,
                                    'class' => 'form-control',
                                    'value' => $this->App->checkParam('string')
                                ));
                                ?>
                                <button type="submit" class="btn btn-info">Search</button>
                            </div>
                        </div>
                    </div>
                    </br>
                <?php endif; ?>
                <div class="table-responsive">
                    <table class="table table-condensed">
                        <thead style="font-weight: bold;">
                            <tr>
                                <td><?php echo $this->Paginator->sort('barcode_number', 'Barcode #'); ?></td>
                                <td><?php echo $this->Paginator->sort('item_name', 'Item name'); ?></td>
                                <td><?php echo $this->Paginator->sort('quantity', 'Available unit(s)'); ?></td>
                                <td><?php echo $this->Paginator->sort('price', 'Price/unit'); ?></td>
                                <td><?php echo $this->Paginator->sort('storage_number', 'Storage Area'); ?></td>
                                <td><?php echo $this->Paginator->sort('created_date', 'Date Added'); ?></td>
                                <td><?php echo $this->Paginator->sort('updated_date', 'Date Updated'); ?></td>
                                <td><?php echo $this->Paginator->sort('created_by', 'Added by'); ?></td>
                                <td><?php echo $this->Paginator->sort('updated_by', 'Updated by'); ?></td>
                                <td>Actions</td>
                            </tr>
                        </thead>
                        <tbody class="table-hover">
                            <?php foreach ($Items as $item): ?>
                                <tr <?php if ($item['Item']['flags'] == 1) { ?>style="background-color:rgb(251,245,216);"<?php } else if ($item['Item']['flags'] == 2) { ?>style="background-color:rgb(238,212,212);"<?php }; ?>>
                                    <td>
                                        <?php
                                        echo $this->Html->link(
                                                $item['Item']['barcode_number'], array(
                                            'controller' => 'items',
                                            'action' => 'view_item_details',
                                            $item['Item']['id']
                                                )
                                        );
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        echo $this->Html->link(
                                                $item['Item']['item_name'], array(
                                            'controller' => 'items',
                                            'action' => 'view_item_details',
                                            $item['Item']['id']
                                                )
                                        );
                                        ?>
                                    </td>
                                    <td><?php echo h($item['Item']['quantity']); ?></td>
                                    <td><i class="fa fa-rub"></i> <?php echo h($item['Item']['price']); ?></td>
                                    <td><?php echo h($item['Item']['storage_number']); ?></td>
                                    <td><?php echo h($item['Item']['created_date']); ?></td>
                                    <td><?php echo h($item['Item']['updated_date']); ?></td>
                                    <td><?php echo h($item['Created_by']['first_name'] . ' ' . $item['Created_by']['last_name']); ?></td>
                                    <td><?php echo h($item['Updated_by']['first_name'] . ' ' . $item['Updated_by']['last_name']); ?></td>
                                    <td>
                                        <?php
                                        echo $this->Html->link(
                                                '<span class="badge actions-edit"><i class="fa fa-edit"></i></span>', array(
                                            'action' => 'edit_item',
                                            $item['Item']['id']
                                                ), array(
                                            'escape' => false,
                                            'title' => 'Edit this item'
                                                )
                                        );
                                        ?>
                                         | 
                                        <?php
                                        echo $this->Html->link(
                                                '<span class="badge actions-delete"><i class="fa fa-trash-o"></i></span>', array('controller' => 'items',
                                            'action' => 'delete_item',
                                            $item['Item']['id']
                                                ), array(
                                            'escape' => false,
                                            'title' => 'Delete this item',
                                            'confirm' => 'Are you sure you want to delete this item?'
                                                )
                                        );
                                        ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="panel-heading">
                <?php echo $this->element('paginator'); ?>                
            </div>
        </div>
    </div>
</div>