<style>
    div.header{
        color:#575757;
        font-size: 0.9em;
    }
    small.text-muted{
        font-size: 0.7em;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary panel-chat">
            <div class="panel-heading">
                <span class="glyphicon glyphicon-comment"></span> Chat
                <div class="btn-group pull-right">
                    <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                        <span class="glyphicon glyphicon-chevron-down"></span>
                    </button>
                    <ul class="dropdown-menu slidedown">
                        <li><a href="http://www.jquery2dotnet.com"><span class="glyphicon glyphicon-refresh">
                                </span>Refresh</a></li>
                        <li><a href="http://www.jquery2dotnet.com"><span class="glyphicon glyphicon-ok-sign">
                                </span>Available</a></li>
                        <li><a href="http://www.jquery2dotnet.com"><span class="glyphicon glyphicon-remove">
                                </span>Busy</a></li>
                        <li><a href="http://www.jquery2dotnet.com"><span class="glyphicon glyphicon-time"></span>
                                Away</a></li>
                        <li class="divider"></li>
                        <li><a href="http://www.jquery2dotnet.com"><span class="glyphicon glyphicon-off"></span>
                                Sign Out</a></li>
                    </ul>
                </div>
            </div>
            <div class="panel-body" id="panel-body">
                <ul class="chat" id="chat-body">
                    <?php
                    foreach ($messages as $message) :
                        if ($authUser['id'] == $message['Message']['from']) {
                            ?>
                            <li class="right clearfix"><span class="chat-img pull-right">
                                    <img src="http://graph.facebook.com/djvincent25/picture" alt="User Avatar" class="img-circle" />
                                </span>
                                <div class="chat-body clearfix">
                                    <div class="header">
                                        <strong class="pull-right primary-font">
                                            <?php echo $message['From']['first_name'] . ' ' . $message['From']['last_name']; ?>
                                        </strong>
                                        </br>
                                    </div>
                                    <p class="pull-right message-body"><?php echo ($message['Message']['body']) ?></p>
                                </div>
                                <small class="pull-right text-muted"><?php echo $this->App->formatTime($message['Message']['created_date']) . '   '; ?><span class="glyphicon glyphicon-time"></span></small>
                            </li>
                        <?php } else { ?>
                            <li class="left clearfix"><span class="chat-img pull-left">
                                    <img src="http://graph.facebook.com/WonderfulEngineering/picture" alt="User Avatar" class="img-circle" />
                                </span>
                                <div class="chat-body clearfix">
                                    <div class="header">
                                        <strong class="primary-font">
                                            <?php echo $message['From']['first_name'] . ' ' . $message['From']['last_name']; ?>
                                        </strong>
                                    </div>
                                    <p class="message-body"><?php echo h($message['Message']['body']) ?></p>
                                </div>
                                <small class="text-muted"><span class="glyphicon glyphicon-time"></span><?php echo $this->App->formatTime($message['Message']['created_date']) . '   '; ?></small>
                            </li>
                        <?php } endforeach; ?>
                </ul>
            </div>
            <div class="panel-footer">
                <?php
                echo $this->Form->create('', array('onsubmit' => 'return false;'));
                ?>
                <div class="input-group">
                    <input id="btn-input" type="text" class="form-control input-sm" placeholder="Type your message here..." />
                    <span class="input-group-btn">
                        <button class="btn btn-warning btn-sm" id="btn-chat" type="submit">
                            Send
                        </button>
                    </span>
                </div>
                <?php echo $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>

<?php #echo $this->element('sql_dump');  ?>

<style type="text/css">
    .panel{
        height:100%;
    }
</style>


<script type="text/javascript">
    $(document).ready(function() {
        scrollMessageDown();
        setInterval(function() {
            update_chat_box_messages();
        },1000);

    });
    $(document).keypress(function(e) {
        if (e.which == 13) {
            submitChat();
        }
    });


    function scrollMessageDown() {
        if ($('#panel-body').scrollTop() + $('#panel-body').height() != $(document).height()) {
            $("#panel-body").animate({scrollTop: $('#panel-body')[0].scrollHeight}, 1000);
        }
    }

    function submitChat() {
        if ($('#btn-input').val() == 0 || $('#btn-input').val() == null || $('#btn-input').val() == '') {
            return false;
        }

        $.ajax({
            type: "POST",
            data: {
                message: $('#btn-input').val(),
                from: "<?php echo $from; ?>",
                to: "<?php echo $to; ?>"
            },
            url: "<?php echo $this->App->routes('messages', 'new_message') ?>",
            success: function(data) {
                $('#btn-input').val('');
                $('ul#chat-body').html(data);
                scrollMessageDown();
            },
            error: function(data) {
                console.log(data);
            }
        });
        return false;
    }

    function update_chat_box_messages() {
        $.ajax({
            type: "POST",
            data: {
                message: '',
                from: "<?php echo $from; ?>",
                to: "<?php echo $to; ?>"
            },
            url: "<?php echo $this->App->routes('messages', 'new_message') ?>",
            success: function(data) {
                $('ul#chat-body').html(data);
            },
            error: function(data) {
                console.log(data);
            }
        });
    }
</script>