<div class="row">
    <div class="col-lg-12 height-150 border-blue">
    </div>
</div>
<div class="col-lg-1">
    <div class="panel panel-info">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-6">
                    <i class="fa fa-plus-square fa-5x"></i>
                </div>
                <div class="col-xs-6 text-right">
                    <p class="announcement-heading" id="new_added_items">0</p>
                    <p class="announcement-text">New items added to record</p>
                </div>
            </div>
        </div>
        <a href="#">
            <div class="panel-footer announcement-bottom">
                <div class="row">
                    <div class="col-xs-6">
                        <?php
                        echo $this->Html->link('View Items', array('controller' => 'Items', 'action' => 'view_new_added_items'))
                        ?>
                    </div>
                    <div class="col-xs-6 text-right">
                        <i class="fa fa-arrow-circle-right"></i>
                    </div>
                </div>
            </div>
        </a>
    </div>
</div>
<div class="col-lg-1">
    <div class="panel panel-info">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-6">
                    <i class="fa fa-plus-square fa-5x"></i>
                </div>
                <div class="col-xs-6 text-right">
                    <p class="announcement-heading" id="new_added_items">0</p>
                    <p class="announcement-text">New items added to record</p>
                </div>
            </div>
        </div>
        <a href="#">
            <div class="panel-footer announcement-bottom">
                <div class="row">
                    <div class="col-xs-6">
                        <?php
                        echo $this->Html->link('View Items', array('controller' => 'Items', 'action' => 'view_new_added_items'))
                        ?>
                    </div>
                    <div class="col-xs-6 text-right">
                        <i class="fa fa-arrow-circle-right"></i>
                    </div>
                </div>
            </div>
        </a>
    </div>
</div>