<style>
    hr{
        border:1px solid #616161;
    }
</style>

<div class="row">
    <div class="col-lg-12">
        <h1><small>View </small> Todays <small>current logs</small></h1>
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            The items that is shown below is current system logs and transactions
        </div>
    </div>
</div><!-- /.row -->

<div  id="no-current-log" class="row" style="<?php echo $todaysLog ? "display:none;" : ""; ?>">
    <div class="col-lg-12 " style="text-align:center;font-size: 12px;color:rgb(58,131,194);">
        <i class="fa fa-minus-circle"></i>&nbsp;No items can be displayed
    </div>
</div>

<hr>
<div class="row">
    <div class="col-lg-12" id="today_logs">
        <?php foreach ($todaysLog as $logstoday) : ?>

            <div class="row ">
                <div class="col-lg-12">
                    <img src="http://graph.facebook.com/lychell712012/picture" style="border:2px solid black;">&nbsp;&nbsp;&nbsp;
                    <strong><?php echo $logstoday['User']['first_name'] . ' ' . $logstoday['User']['last_name']; ?></strong>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <p style="margin-left: 70px;"><i class="fa fa-angle-right"></i>&nbsp;<?php echo h($logstoday['Log']['event_log']); ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12" style="font-size: 10px;">
                    <em>Even log created on: <?php echo $logstoday['Log']['created_date']; ?></em>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12" style="text-align:right;font-size: 12px;">
                    <a href="#">View Comments</a> | <a href="#">Add Comment</a>
                </div>
            </div>
            <hr>

        <?php endforeach; ?>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        setInterval(
                function() {
                    $.ajax({
                        type: "POST",
                        url: "query_new_added_logs",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function(data) {
                            if (data.new_log != null) {
                                $(data.new_log).prependTo('#today_logs').effect('shake').effect('highlight', {}, 5000);
                                $('#no-current-log').css('display', 'none');
                            }
                        },
                        error: function(data) {
                            //console.log(data);
                        }
                    });
                }
        , 3000);
    })
</script>