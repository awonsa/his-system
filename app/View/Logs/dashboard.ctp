<style>
    hr{
        border:1px solid #616161;
    }
    #today_logs{
        max-height: 280px;
        overflow-y: auto;

    }
</style>

<div class="row">
    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-6">
                <h1>Dashboard <small>Statistics Overview</small></h1>
            </div>
            <div class="col-lg-6">
                </br>
                <?php echo $this->element('item_search'); ?>
            </div>
        </div>
        <ol class="breadcrumb">
            <li class="active"><i class="fa fa-dashboard"></i> Dashboard</li>
        </ol>
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            Welcome to HIS System by 
            <a class="alert-link" href="#">SAMPLE Soft</a>! 
            Feel free to use this template for your admin needs! We are using a 
            few different plugins to handle the dynamic tables and charts, so 
            make sure you check out the necessary documentation links provided.
        </div>
    </div>
</div><!-- /.row -->

</br>
<div class="row">
    <div class="col-lg-3">
        <div class="panel panel-success">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-6">
                        <i class="fa fa-comments fa-5x"></i>
                    </div>
                    <div class="col-xs-6 text-right">
                        <p class="announcement-heading" id="new_transactions_count">0</p>
                        <p class="announcement-text">Todays new transactions</p>
                    </div>
                </div>
            </div>
            <a href="#">
                <div class="panel-footer announcement-bottom">
                    <div class="row">
                        <div class="col-xs-6">
                            <?php
                            echo $this->Html->link('View transactions', array('controller' => 'Logs', 'action' => 'view_todays_transaction'))
                            ?>
                        </div>
                        <div class="col-xs-6 text-right">
                            <i class="fa fa-arrow-circle-right"></i>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-6">
                        <i class="fa fa-plus-square fa-5x"></i>
                    </div>
                    <div class="col-xs-6 text-right">
                        <p class="announcement-heading" id="new_added_items">0</p>
                        <p class="announcement-text">New items added to record</p>
                    </div>
                </div>
            </div>
            <a href="#">
                <div class="panel-footer announcement-bottom">
                    <div class="row">
                        <div class="col-xs-6">
                            <?php
                            echo $this->Html->link('View Items', array('controller' => 'Items', 'action' => 'view_new_added_items'))
                            ?>
                        </div>
                        <div class="col-xs-6 text-right">
                            <i class="fa fa-arrow-circle-right"></i>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>

    <div class="col-lg-3">
        <div class="panel panel-warning">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-6">
                        <i class="fa fa-check fa-5x"></i>
                    </div>
                    <div class="col-xs-6 text-right">
                        <p class="announcement-heading" id="almost_out_of_stock">0</p>
                        <p class="announcement-text">Products almost out of stock</p>
                    </div>
                </div>
            </div>
            <a href="#">
                <div class="panel-footer announcement-bottom">
                    <div class="row">
                        <div class="col-xs-6">
                            <?php
                            echo $this->Html->link('View items', array('controller' => 'Items', 'action' => 'view_items_almost_out_of_stock'))
                            ?>
                        </div>
                        <div class="col-xs-6 text-right">
                            <i class="fa fa-arrow-circle-right"></i>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>

    <div class="col-lg-3">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-6">
                        <i class="fa fa-exclamation-triangle fa-5x"></i>
                    </div>
                    <div class="col-xs-6 text-right">
                        <p class="announcement-heading" id="out_of_stock">0</p>
                        <p class="announcement-text">Products out of stock</p>
                    </div>
                </div>
            </div>
            <a href="#">
                <div class="panel-footer announcement-bottom">
                    <div class="row">
                        <div class="col-xs-6">
                            <?php
                            echo $this->Html->link('View items', array('controller' => 'Items', 'action' => 'view_items_out_of_stock'))
                            ?>
                        </div>
                        <div class="col-xs-6 text-right">
                            <i class="fa fa-arrow-circle-right"></i>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>

</div><!-- /.row -->

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Transaction Logs</h3>
            </div>
            <div class="panel-body" id="today_logs">

                <?php foreach ($todaysLog as $logstoday) : ?>

                    <div class="row ">
                        <div class="col-lg-12">
                            <img src="http://graph.facebook.com/djvincent25/picture" style="border:2px solid black;">&nbsp;&nbsp;&nbsp;
                            <strong><?php echo $logstoday['User']['first_name'] . ' ' . $logstoday['User']['last_name']; ?></strong>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <p style="margin-left: 70px;"><i class="fa fa-angle-right"></i>&nbsp;<?php echo h($logstoday['Log']['event_log']); ?></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12" style="font-size: 10px;">
                            <em>Event log created on: <?php echo $logstoday['Log']['created_date']; ?></em>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12" style="text-align:right;font-size: 12px;">
                            <a href="#">View Comments</a> | <a href="#">Add Comment</a>
                        </div>
                    </div>
                    <hr>

                <?php endforeach; ?>

                <div class="row">
                    <div class="col-lg-12" style="text-align:center;font-size: 12px;">
                        <i class="fa fa-angle-double-down"></i>&nbsp;<a href="#">Show more</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div><!-- /.row -->



<script type="text/javascript">
    $(document).ready(function() {
        updateDashboardLogs();
        setInterval(function() {
            updateDashboardLogs();
        }, 5000);
    });

    function updateDashboardLogs() {
        $.ajax({
            type: "POST",
            url: "<?php echo $this->App->routes('logs', 'query_new_added_logs') ?>",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(data) {
                $('#new_transactions_count').html(data.count_logs);
                $('#new_added_items').html(data.count_new_added_items);
                $('#almost_out_of_stock').html(data.count_almost_out_of_stock);
                $('#out_of_stock').html(data.count_out_of_stock);
                if (data.new_log != null) {
                    $(data.new_log).prependTo('#today_logs').effect('shake').effect('highlight', {}, 5000);
                } else {
                    $('#today_logs').prepend(data.new_log);
                }
            },
            error: function(data) {
            }
        });
    }
</script>

<!--

<div class="row">
    <div class="col-lg-4">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-long-arrow-right"></i> Traffic Sources: October 1, 2013 - October 31, 2013</h3>
            </div>
            <div class="panel-body">
                <div id="morris-chart-donut"></div>
                <div class="text-right">
                    <a href="#">View Details <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-clock-o"></i> Recent Activity</h3>
            </div>
            <div class="panel-body">
                <div class="list-group">
                    <a href="#" class="list-group-item">
                        <span class="badge">just now</span>
                        <i class="fa fa-calendar"></i> Calendar updated
                    </a>
                    <a href="#" class="list-group-item">
                        <span class="badge">4 minutes ago</span>
                        <i class="fa fa-comment"></i> Commented on a post
                    </a>
                    <a href="#" class="list-group-item">
                        <span class="badge">23 minutes ago</span>
                        <i class="fa fa-truck"></i> Order 392 shipped
                    </a>
                    <a href="#" class="list-group-item">
                        <span class="badge">46 minutes ago</span>
                        <i class="fa fa-money"></i> Invoice 653 has been paid
                    </a>
                    <a href="#" class="list-group-item">
                        <span class="badge">1 hour ago</span>
                        <i class="fa fa-user"></i> A new user has been added
                    </a>
                    <a href="#" class="list-group-item">
                        <span class="badge">2 hours ago</span>
                        <i class="fa fa-check"></i> Completed task: "pick up dry cleaning"
                    </a>
                    <a href="#" class="list-group-item">
                        <span class="badge">yesterday</span>
                        <i class="fa fa-globe"></i> Saved the world
                    </a>
                    <a href="#" class="list-group-item">
                        <span class="badge">two days ago</span>
                        <i class="fa fa-check"></i> Completed task: "fix error on sales page"
                    </a>
                </div>
                <div class="text-right">
                    <a href="#">View All Activity <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-money"></i> Recent Transactions</h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped tablesorter">
                        <thead>
                            <tr>
                                <th>Order # <i class="fa fa-sort"></i></th>
                                <th>Order Date <i class="fa fa-sort"></i></th>
                                <th>Order Time <i class="fa fa-sort"></i></th>
                                <th>Amount (USD) <i class="fa fa-sort"></i></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>3326</td>
                                <td>10/21/2013</td>
                                <td>3:29 PM</td>
                                <td>$321.33</td>
                            </tr>
                            <tr>
                                <td>3325</td>
                                <td>10/21/2013</td>
                                <td>3:20 PM</td>
                                <td>$234.34</td>
                            </tr>
                            <tr>
                                <td>3324</td>
                                <td>10/21/2013</td>
                                <td>3:03 PM</td>
                                <td>$724.17</td>
                            </tr>
                            <tr>
                                <td>3323</td>
                                <td>10/21/2013</td>
                                <td>3:00 PM</td>
                                <td>$23.71</td>
                            </tr>
                            <tr>
                                <td>3322</td>
                                <td>10/21/2013</td>
                                <td>2:49 PM</td>
                                <td>$8345.23</td>
                            </tr>
                            <tr>
                                <td>3321</td>
                                <td>10/21/2013</td>
                                <td>2:23 PM</td>
                                <td>$245.12</td>
                            </tr>
                            <tr>
                                <td>3320</td>
                                <td>10/21/2013</td>
                                <td>2:15 PM</td>
                                <td>$5663.54</td>
                            </tr>
                            <tr>
                                <td>3319</td>
                                <td>10/21/2013</td>
                                <td>2:13 PM</td>
                                <td>$943.45</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="text-right">
                    <a href="#">View All Transactions <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>

-->