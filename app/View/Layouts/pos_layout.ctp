<!DOCTYPE html>
<html>
    <head>

        <?php echo $this->Html->charset(); ?>

        <title><?php echo $title_for_layout; ?></title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <?php
        echo $this->Html->css('font-awesome/css/font-awesome.min.css');
        echo $this->Html->css('bootstrap.min.css');
        echo $this->Html->css('bootstrap-theme.min.css');
        echo $this->Html->css('style.css');
        echo $this->Html->css('morris-0.4.3.min.css');
        echo $this->Html->css('sb-admin.css');
        echo $this->Html->css('jquery-ui-1.10.4.custom.css');
        echo $this->Html->script('jquery.js');
        echo $this->Html->script('jquery-ui-1.10.4.custom.js');
        echo $this->fetch('meta');
        echo $this->fetch('css');
        ?>

    </head>
    <body style="background-color: #E8E9E9;margin-top:0px;">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <?php echo $this->Session->flash(); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <?php echo $this->fetch('content'); ?>
                </div>
            </div>
        </div>
        <?php
        echo $this->element('modal');
        ?>
<!--<div class="row"><?php echo $this->element('sql_dump'); ?></div>-->

    </body>
    <?php
    echo $this->Html->script('bootstrap.js');
    echo $this->Html->script('morris/chart-data-morris.js');
    echo $this->Html->script('tablesorter/jquery.tablesorter.js');
    echo $this->Html->script('tablesorter/tables.js');
    echo $this->Html->script('raphael-min.js');
    echo $this->Html->script('morris-0.4.3.min.js');
    echo $this->fetch('script');
    ?>


    <script type='text/javascript'>
        $(function() {
            $("#info_modal").dialog({autoOpen: false});
        });
    </script>

</html>
