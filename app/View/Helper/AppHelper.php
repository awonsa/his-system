<?php

/**
 * Application level View Helper
 *
 * This file is application-wide helper file. You can put all
 * application-wide helper-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Helper
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Helper', 'View');

/**
 * Application helper
 *
 * Add your application-wide methods in the class below, your helpers
 * will inherit them.
 *
 * @package       app.View.Helper
 */
class AppHelper extends Helper {

    public function gender($gender) {
        if ($gender == 0) {
            return 'Male';
        }
        return 'Female';
    }

    public function checkParam($name) {
        if (isset($this->params['url'][$name])) {
            return $this->params['url'][$name];
        }
        return false;
    }

    public function checkTabifActive($url = null) {
        $actual_url = "$_SERVER[REQUEST_URI]";
        if (strpos($actual_url, $url) !== false) {
            return 'active';
        }
        return null;
    }

    public function routes($controller = null, $action = null, $id = null) {
        if (empty($controller) || empty($action)) {
            return null;
        }
        $route = Router::url(array('controller' => $controller, 'action' => $action)) . '/' . $id;
        return $route;
    }

    public function formatTime($time = null) {
        return date('l - F j, Y - h:i:s A', strtotime($time));
    }

}
