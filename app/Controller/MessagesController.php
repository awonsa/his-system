<?php

class MessagesController extends AppController {

    public function request_message_update() {
        $this->autoRender = false;
        $this->request->onlyAllow('ajax');
        $result = $this->Message->queryMessageUpdate($this->Auth->user('id'), 7);
        return json_encode($result);
    }

    public function new_message() {
        $this->autoRender = false;
        $this->request->onlyAllow('ajax');
        $result = $this->Message->saved_new_message($this->request->data);
        return $result;
    }

    public function message_view($id = null, $from = null) {
        if ($id == null) {
            $this->redirect('view_all_message');
        }
        $this->set('messages', $this->Message->getAllMessageFromAUser($from, $this->Auth->user('id'), 20));
        $this->set('from', $this->Auth->user('id'));
        $this->set('to', $from);
    }

    public function view_all_message() {
        
    }

}
