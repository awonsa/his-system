<?php

class ItemsController extends AppController {

    public $components = array('Paginator');
    public $uses = array('Item', 'QuantityType', 'Log');
    public $helpers = array('App');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->set_user_loggedin();
        $this->Auth->allow();
    }

    public function index() {
        $request = $this->request->here;
        $string = $this->checkParam('string');
        $conditions = array();
        $conditions[] = $this->Item->searchConditions($string);
        $conditions[] = array(
            'Item.is_deleted =' => 0
        );

        if ($request !== '/inventory_system/items/add_item') {
            $paginate = array(
                'limit' => 20,
                'order' => array(
                    'Item.barcode_number' => 'asc'
                ),
                'conditions' => $conditions
            );

            $this->Paginator->settings = $paginate;
            $this->Item->recursive = 0;
            $data = $this->Paginator->paginate('Item');
            $this->set('Items', $data);
            $this->set('enable_search', true);
        }
    }

    public function add_item() {
        $this->set('quantityType', $this->QuantityType->find('list', array('fields' => array('QuantityType.id', 'QuantityType.quantity_type'), 'order' => 'quantity_type ASC')));
        if ($this->request->is('post')) {
            $result = $this->Item->check_item_if_exist(array('conditions' => array(
                    'barcode_number' => $this->request->data['Item']['barcode_number'])));
            if ($result === true) {
                $this->request->data['Item']['created_date'] = $this->Item->get_current_time();
                $this->request->data['Item']['updated_date'] = $this->Item->get_current_time();
                $this->request->data['Item']['created_by'] = $this->Auth->user()['id'];
                $this->Item->create();
                if ($this->Item->save($this->request->data)) {
                    $data = $this->request->data;
                    $this->success_message('Item has been saved successfully!');
                    $this->Log->create_log('Item Name: ' . $data['Item']['item_name'] . ' - Item Barcode: ' . $data['Item']['barcode_number'] . ' -- Has added to records successfully', $this->Auth->user()['id']);
                    return $this->redirect(array('controller' => 'items', 'action' => 'add_item'));
                } else {
                    $this->error_message('Error in adding your item in the database. Please try again');
                    return $this->redirect(array('controller' => 'items', 'action' => 'add_item'));
                }
            } else {
                $this->error_message('The item your are trying to add is already exist in record. Just click edit if you want to add more quantities!');
                $paginate = array(
                    'limit' => 20,
                    'order' => array(
                        'Item.barcode_number' => 'asc'
                    ),
                    'conditions' => array('barcode_number' => $this->request->data['Item']['barcode_number'], 'Item.is_deleted =' => 0)
                );

                $this->Paginator->settings = $paginate;
                $this->Item->recursive = 0;
                $data = $this->Paginator->paginate('Item');
                $this->set('Items', $data);
                $this->setAction('index');
            }
        }
    }

    public function edit_item($id = null) {
        $this->set('quantityType', $this->QuantityType->find('list', array('fields' => array('QuantityType.id', 'QuantityType.quantity_type'), 'order' => 'quantity_type ASC')));
        if ($id == null) {
            $this->redirect('/');
        }
        $item = $this->Item->find('all', array(
            'conditions' => array(
                'Item.id' => $id,
                'Item.is_deleted' => 0
            ),
            'recursive' => -1
                )
        );
        if (empty($item)) {
            $this->error_message('Item not found from the records');
            $this->redirect(array('action' => 'index'));
        }

        if ($this->request->is(array('post', 'put'))) {
            $Auth = $this->Auth->user();
            $this->Item->id = $id;
            $this->request->data['Item']['updated_by'] = $Auth['id'];
            $this->request->data['Item']['updated_date'] = $this->Item->get_current_time();
            if ($this->Item->save($this->request->data)) {
                $data = $this->request->data;
                $this->success_message('Item updated successfully');
                $this->Log->create_log('Item Name: ' . $data['Item']['item_name'] . ' - Item Barcode: ' . $data['Item']['barcode_number'] . ' -- Has updated item record successfully', $this->Auth->user()['id']);
                return $this->redirect(array('action' => 'index'));
            }
            $this->error_message('Error in updating record. Please try again later');
        }

        if (empty($this->request->data)) {
            $this->request->data = $item;
        }
    }

    public function view_new_added_items() {
        $string = $this->checkParam('string');
        $paginate = array(
            'limit' => 20,
            'order' => array(
                'Item.barcode_number' => 'asc'
            ),
            'conditions' => array(
                'Item.created_date >=' => date('Y-m-d', strtotime('-3 day')) . '%',
                $this->Item->searchConditions($string),
                'Item.is_deleted =' => 0
            )
        );
        $this->Paginator->settings = $paginate;
        $this->Item->recursive = 0;
        $data = $this->Paginator->paginate('Item');
        $this->set('Items', $data);
    }

    public function view_items_almost_out_of_stock() {
        $string = $this->checkParam('string');
        $paginate = array(
            'limit' => 20,
            'order' => array(
                'Item.barcode_number' => 'asc'
            ),
            'conditions' => array('Item.flags =' => 1,
                $this->Item->searchConditions($string),
                'Item.is_deleted =' => 0
            )
        );
        $this->Paginator->settings = $paginate;
        $this->Item->recursive = 0;
        $data = $this->Paginator->paginate('Item');
        $this->set('Items', $data);
    }

    public function view_items_out_of_stock() {
        $string = $this->checkParam('string');
        $paginate = array(
            'limit' => 20,
            'order' => array(
                'Item.barcode_number' => 'asc'
            ),
            'conditions' => array('Item.flags =' => 2,
                $this->Item->searchConditions($string),
                'Item.is_deleted =' => 0
            )
        );
        $this->Paginator->settings = $paginate;
        $this->Item->recursive = 0;
        $data = $this->Paginator->paginate('Item');
        $this->set('Items', $data);
    }

    public function view_item_details($id = null) {
        if ($id == null) {
            return $this->redirect('/');
        }
        $paginate = array(
            'limit' => 20,
            'order' => array(
                'Item.barcode_number' => 'asc'
            ),
            'conditions' => array(
                'Item.id' => h($id),
                'Item.is_deleted =' => 0
            )
        );

        $this->Paginator->settings = $paginate;
        $this->Item->recursive = 0;
        $data = $this->Paginator->paginate('Item');
        if (empty($data)) {
            $this->redirect('/');
            exit;
        }
        $this->set('Items', $data);
    }

    public function delete_item($id = null) {
        if ($id == null) {
            $this->redirect('index');
        }
        $this->autoRender = false;
        $data = $this->Item->find('all', array(
            'conditions' => array('Item.id =' => $id, 'Item.is_deleted =' => 0),
            'recursive' => -1
        ));
        $result = $this->Item->updateAll(array('Item.is_deleted' => 1), array('Item.id =' => $id, 'Item.is_deleted =' => 0));
        if ($result) {
            $this->success_message('Item deleted successfully');
            $this->Log->create_log('Item Name: ' . $data[0]['Item']['item_name'] . ' - Item Barcode: ' . $data[0]['Item']['barcode_number'] . ' -- Has deleted item record successfully', $this->Auth->user()['id']);
        } else {
            $this->error_message('Error in deleting item. Item may not exist. Please try again later');
        }
        $this->redirect(array('actions' => 'index'));
    }

}
