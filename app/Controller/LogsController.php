<?php

App::uses('AppController', 'Controller');

class LogsController extends AppController {

    public $uses = array('Log', 'Item');

    public function beforeFilter() {
        parent::beforeFilter();
//        if (!isset($this->Auth->user)) {
//            $this->redirect(array('controller' => 'users', 'action' => 'login'));
//        }
    }

    public function dashboard() {
        $this->set('todaysLog', $this->Log->find('all', array(
                    'order' => 'Log.created_date DESC',
                    'limit' => 10
        )));
    }

    public function query_new_added_logs() {
        $this->autoRender = false;
        $this->request->onlyAllow('ajax');
        $result = $this->Log->new_logs();
        return json_encode($result);
    }

    public function view_todays_transaction() {
        $this->set('todaysLog', $this->Log->find('all', array(
                    'order' => 'Log.created_date DESC',
                    'conditions' => array('Log.created_date LIKE' => date('Y-m-d') . '%')
        )));
    }

}
