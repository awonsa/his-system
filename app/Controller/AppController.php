<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    public $components = array(
        'Session',
        'Auth' => array(
            'loginRedirect' => array(
                'controller' => 'logs',
                'action' => 'dashboard'
            ),
            'logoutRedirect' => array(
                'controller' => 'users',
                'action' => 'login',
            )
        )
    );

    public function beforeFilter() {
        $this->Auth->allow('login');
        $this->set_user_loggedin();
    }

    public function error_message($message = null) {
        return $this->Session->setFlash(__($message), 'error_flash');
    }

    public function success_message($message = null) {
        return $this->Session->setFlash(__($message), 'success_flash');
    }

    public function set_user_loggedin() {
        $this->set('authUser', $this->Auth->user());
    }

    public function checkParam($name) {
        if (isset($this->params['url'][$name])) {
            return $this->params['url'][$name];
        }
        return null;
    }

    public function routes($controller = null, $action = null, $id = null) {
        if (empty($controller) || empty($action)) {
            return null;
        }
        $route = Router::url(array('controller' => $controller, 'action' => $action)) . '/' . $id;
        return $route;
    }

}
