<?php

class Message extends AppModel {

    public $belongsTo = array(
        'From' => array(
            'className' => 'User',
            'foreignKey' => 'from'
        ),
        'To' => array(
            'className' => 'User',
            'foreignKey' => 'to'
        )
    );

    public function queryMessageUpdate($member_id = null, $limit = null) {

        if ($member_id == null || $limit == null) {
            return null;
        }

        $result = array();

        $sql = "SELECT `Message`.`id`, `Message`.`body`, `Message`.`is_new`, `Message`.`from`,`Message`.`created_date`,  `From`.`first_name`, `From`.`last_name`, `To`.`first_name`, `To`.`last_name` 
                FROM `his_system`.`messages` AS `Message` 
                INNER JOIN (
                        SELECT MAX(`created_date`) AS `created_date`
                        FROM `messages`
                        WHERE `to` = " . $member_id . "
                        GROUP BY `from`
                ) AS `MaxDate` ON (`Message`.`created_date` = `MaxDate`.`created_date`) 
                LEFT JOIN `his_system`.`users` AS `From` 
                        ON (`Message`.`from` = `From`.`id`) 
                LEFT JOIN `his_system`.`users` AS `To` 
                        ON (`Message`.`to` = `To`.`id`) 
                WHERE `Message`.`to` = " . $member_id . " 
                GROUP BY `Message`.`from` 
                ORDER BY `Message`.`is_new` DESC,`Message`.`created_date` DESC
                LIMIT " . $limit;

        $tmp = $this->find('count', array(
            'conditions' => array('Message.to' => $member_id, 'Message.is_new' => 1),
            'group' => 'Message.from',
            'recursive' => -1
        ));

        $result['new_message_count'] = $tmp;
        $tmp = $this->query($sql);
        $html = '<li class="dropdown-header"><span id="number_of_new_message_2">' . $result['new_message_count'] . '</span> New Messages</li>';

        if (!empty($tmp)) {

            foreach ($tmp as $data) {
                $icon = null;
                if ($data['Message']['is_new'] == 1) {
                    $icon = '&nbsp;&nbsp;&nbsp;<i class="fa fa-comment" style="color:green;"></i>';
                }
                $html = $html . "<li class='divider'></li>
                    <li class='message-preview'>
                        <a href='" . $this->routes('messages', 'message_view', $data['Message']['id'] . '/' . $data['Message']['from']) . "'>
                            <span class='avatar'><img src='http://placehold.it/50x50'></span>
                            <span class='name'>" . $data['From']['first_name'] . ' ' . $data['From']['last_name'] . "</span>
                            <span class='message'>" . $data['Message']['body'] . "</span>
                            <span class='time'><i class='fa fa-clock-o'></i> " . date('h:i A', strtotime($data['Message']['created_date'])) . $icon . "</span>
                        </a>
                    </li>
                    ";
            }
            $html = $html . '<li class="divider"></li>
                                <li style="text-align:center;">
                                    <a href="#">View All Messages</a>
                                </li>';
        }

        $result['message_preview'] = $html;

        return $result;
    }

    public function getAllMessageFromAUser($from = null, $id = null, $limit = null) {
        if ($from == null || $id == null) {
            return false;
        }
        $conditions = array();

        $conditions['OR'] = array(
            '(`Message`.`from` = ' . $from . ' AND  `Message`.`to` = ' . $id . ')',
            '(`Message`.`from` = ' . $id . ' AND  `Message`.`to` = ' . $from . ')'
        );

        $result = $this->find('all', array(
            'fields' => array('Message.id', 'Message.body', 'Message.is_new', 'Message.from', 'Message.to', 'Message.created_date', 'From.first_name', 'From.last_name', 'To.first_name', 'To.last_name'),
            'conditions' => $conditions,
            'order' => 'Message.created_date DESC',
            'limit' => $limit
        ));

        $this->updateAll(array('Message.is_new' => 0), array('Message.from =' => $from));
        $result = array_reverse($result);
        return $result;
    }

    public function saved_new_message($data = null) {
        $table_data = array(
            'Message' => array(
                'body' => $data['message'],
                'to' => $data['to'],
                'from' => $data['from'],
                'created_date' => date("Y-m-d H:i:s"),
                'updated_date' => date("Y-m-d H:i:s"),
            )
        );
        if (!empty($data['message'])) {
            $this->create();
            $this->save($table_data);
        }
        $result = $this->getAllMessageFromAUser($data['to'], $data['from'], 20);
        $result_data = $this->format_chat_message($result, $data);
        $this->updateAll(array('Message.is_new' => 0), array('Message.from =' => $data['to']));
        return $result_data;
    }

# ******************************************************************************************
# * 
# * PRIVATE FUNCTIONS BELOW
# *  
# ******************************************************************************************

    private function formatTime($time = null) {
        return date('l - F j, Y - h:i:s A', strtotime($time));
    }

    private function format_chat_message($result = null, $data = null) {
        if (empty($result) || empty($data)) {
            return null;
        }
        $return_data = null;
        foreach ($result as $message) {
            if ($data['from'] == $message['Message']['from']) {
                $return_data .= '<li class="right clearfix"><span class="chat-img pull-right">
                                    <img src="http://graph.facebook.com/djvincent25/picture" alt="User Avatar" class="img-circle" />
                                </span>
                                <div class="chat-body clearfix">
                                    <div class="header">
                                        <strong class="pull-right primary-font">
                                            ' . $message["From"]["first_name"] . ' ' . $message["From"]["last_name"] . '
                                        </strong>
                                        </br>
                                    </div>
                                    <p class="pull-right message-body">' . h($message['Message']['body']) . '</p>
                                </div>
                                <small class="pull-right text-muted"> ' . $this->formatTime($message['Message']['created_date']) . '   <span class="glyphicon glyphicon-time"></span></small>
                            </li>';
            } else {
                $return_data .= '<li class="left clearfix"><span class="chat-img pull-left">
                    <img src="http://graph.facebook.com/WonderfulEngineering/picture" alt="User Avatar" class="img-circle" />
                    </span>
                    <div class="chat-body clearfix">
                        <div class="header">
                            <strong class="primary-font">
                                ' . $message['From']['first_name'] . ' ' . $message['From']['last_name'] . '
                            </strong>
                        </div>
                        <p class="message-body">' . h($message['Message']['body']) . '</p>
                    </div>
                    <small class="text-muted"><span class="glyphicon glyphicon-time"></span>' . $this->formatTime($message['Message']['created_date']) . '   </small>
                    </li>';
            }
        }
        return $return_data;
    }

}
