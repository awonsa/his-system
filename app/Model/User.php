<?php

App::uses('SimplePasswordHasher', 'Controller/Component/Auth');

class User extends AppModel {

    public $hasMany = array(
        'Log' => array(
            'className' => 'Log',
            'condition' => array('Log.user_id' => 'User.id'),
            'order' => 'created_date DESC'
        ),
        'Message_To' => array(
            'className' => 'Message',
            'foreignKey' => 'to',
            'condition' => array('Message.to' => 'User.id'),
            'order' => 'created_date DESC'
        ),
        'Message_From' => array(
            'className' => 'Message',
            'foreignKey' => 'from',
            'condition' => array('Message.from' => 'User.id'),
            'order' => 'created_date DESC'
        ),
        'Item_created' => array(
            'className' => 'Item',
            'foreignKey' => 'created_by',
            'condition' => array('Item.created_by' => 'User.id'),
            'order' => 'created_date DESC'
        ),
        'Item_updated' => array(
            'className' => 'Item',
            'foreignKey' => 'updated_by',
            'condition' => array('Item.updated_by' => 'User.id'),
            'order' => 'created_date DESC'
        )
    );

    public function beforeSave($options = array()) {
        if (isset($this->data[$this->alias]['password'])) {
            $passwordHasher = new SimplePasswordHasher();
            $this->data[$this->alias]['password'] = $passwordHasher->hash(
                    $this->data[$this->alias]['password']
            );
            $this->data[$this->alias]['first_name'] = ucwords(strtolower($this->data[$this->alias]['first_name']));
            $this->data[$this->alias]['last_name'] = ucwords(strtolower($this->data[$this->alias]['last_name']));
        }
        return true;
    }

    public $validate = array(
        'username' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'A username is required'
            )
        ),
        'password' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'A password is required'
            )
        )
    );

}
