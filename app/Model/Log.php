<?php

App::uses('Item', 'Model');

class Log extends AppModel {

    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id'
        )
    );

    public function new_logs() {
        $item = new Item();
        $result = array();
        $result['count_logs'] = $this->find('count', array(
            'fields' => 'Log.id',
            'conditions' => array('Log.created_date LIKE' => date('Y-m-d') . '%')));
        $result['count_almost_out_of_stock'] = $item->find('count', array(
            'fields' => 'Item.flags',
            'conditions' => array('Item.flags =' => 1, 'Item.is_deleted =' => 0)));
        $result['count_out_of_stock'] = $item->find('count', array(
            'fields' => 'Item.flags',
            'conditions' => array('Item.flags =' => 2, 'Item.is_deleted =' => 0)));
        $result['count_new_added_items'] = $item->find('count', array(
            'fields' => 'Item.created_date',
            'conditions' => array('Item.created_date >=' => date('Y-m-d', strtotime('-3 day')) . '%', 'Item.is_deleted =' => 0)));
        $new_log = $this->find('all', array(
            'conditions' => array('is_new =' => 1),
            'order' => 'Log.created_date DESC'
        ));
        if (!empty($new_log)) {
            $result['new_log'] = null;
            foreach ($new_log as $log) {
                $this->id = $log['Log']['id'];
                $this->saveField('is_new', 0);
                $result['new_log'] = $result['new_log'] . '<div class = "row">
                        <div class="col-lg-12">
                                <img src="http://graph.facebook.com/djvincent25/picture" style="border:2px solid black;">&nbsp;&nbsp;&nbsp;
                                <strong>' . $log['User']['first_name'] . ' ' . $log['User']['last_name'] . '</strong>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <p style="margin-left: 70px;"><i class="fa fa-angle-right"></i>&nbsp;' . $log['Log']['event_log'] . '</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12" style="font-size: 10px;">
                                <em>Even log created on: ' . $log['Log']['created_date'] . '</em>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12" style="text-align:right;font-size: 12px;">
                                <a href="#">View Comments</a> | <a href="#">Add Comment</a>
                            </div>
                        </div>
                        <hr>';
            }
        }
        return $result;
    }

    public function create_log($message = null, $id = null) {
        if ($message == null || $id == null) {
            return false;
        }
        $data = array(
            'Log' => array(
                'user_id' => $id,
                'event_log' => $message,
                'created_date' => date('Y-m-d H:i:s'),
            )
        );
        $this->create();
        if ($this->save($data)) {
            return true;
        }
        return false;
    }

}
