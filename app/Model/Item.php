<?php

class Item extends AppModel {

    public $belongsTo = array(
        'Created_by' => array(
            'className' => 'User',
            'foreignKey' => 'created_by'
        ),
        'Updated_by' => array(
            'className' => 'User',
            'foreignKey' => 'updated_by'
        ),
        'Quantity_type' => array(
            'className' => 'QuantityType',
            'foreignKey' => 'quantity_type_id'
        )
    );

    public function searchConditions($string = null) {
        if ($string) {
            $condition = array(
                'OR' => array(
                    'barcode_number LIKE ' => '%' . $string . '%',
                    'item_name LIKE ' => '%' . $string . '%'
                )
            );
            return $condition;
        }
        return null;
    }
    

}
