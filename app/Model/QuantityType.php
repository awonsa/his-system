<?php

class QuantityType extends AppModel {

    public $hasMany = array(
        'Item' => array(
            'className' => 'Item',
            'condition' => array('Item.quantity_type_id' => 'QuantityType.id'),
            'order' => 'created_date DESC'
        ),);

}
